// double-pointer-linked-list.cpp
#include "stdafx.h"

#include <stdlib.h>
#include <stdio.h>

struct list_entry {
	int value;
	struct list_entry* next;
};

struct list_entry*
	ll_create_node(int val = 0)
{
	struct list_entry* entry = (list_entry*) calloc(sizeof(struct list_entry), 1);
	entry->value = val;
	return entry;
}

struct list_entry*
	ll_build_list(int elements)
{
	struct list_entry*  head = ll_create_node(0);
	struct list_entry** iter = &head;
	(void)iter;

	for (int i = 1; i < elements; i++) {
		(*iter)->next = ll_create_node(i);
		iter = &(*iter)->next;
	}

	return head;
}

void
ll_push(struct list_entry* head, int val)
{
	struct list_entry** iter = &head;
	while (*iter) {
		if (nullptr == (*iter)->next) break;
		iter = &(*iter)->next;
	}

	(*iter)->next = ll_create_node(val);
}

struct list_entry*
	ll_pop(struct list_entry** head)
{
	struct list_entry** iter = head;
	while (*iter) {
		if (nullptr == (*iter)->next) break;
		iter = &(*iter)->next;
	}

	if (nullptr == *iter) {
		return nullptr;
	}

	struct list_entry* pop = *iter;
	*iter = pop->next;
	pop->next = nullptr;
	
	return pop;
}

struct list_entry*
	ll_remove(struct list_entry** head, int value)
{
	struct list_entry** iter = head;
	while (*iter && (*iter)->value != value) {
		iter = &(*iter)->next;
	}

	if (nullptr == *iter) {
		return nullptr;
	}

	struct list_entry* element = *iter;
	*iter = element->next;
	element->next = nullptr;

	return element;
}

void
ll_print(struct list_entry* head)
{
	struct list_entry** iter = &head;
	while (*iter) {
		printf("%d ", (**iter).value);
		iter = &(*iter)->next;
	}
	printf("\n");
}

void
ll_pprint(struct list_entry* head)
{
	struct list_entry** iter = &head;
	unsigned long long idx = 1;
	while (*iter) {
		printf("(%lld) [%-4d : %p] ----> [%-4d : %p] (%lld)\n",
			idx,
			(*iter)->value,
			*iter,
			(*iter)->next ? (*iter)->next->value : 0,
			(*iter)->next,
			idx + 1);
		iter = &(*iter)->next;
		idx++;
	}
}


int main()
{
	struct list_entry* head = ll_build_list(10);

	ll_push(head, 100);
	ll_push(head, 200);
	ll_push(head, 300);
	ll_push(head, 400);
	puts("List print");
	ll_print(head);

	struct list_entry* pop = ll_pop(&head);
	printf("POP: %d\n", pop ? pop->value : 0);
	ll_print(head);

	struct list_entry* rem = ll_remove(&head, 200);
	printf("\nREMOVE: %d\n", rem ? rem->value : 0);
	ll_print(head);

	puts("Pretty print");
	ll_pprint(head);

	puts("\nREMOVE HEAD");
	ll_remove(&head, 0);
	ll_pprint(head);
    return 0;
}